import argparse

from PIL import Image


def str2bin(message):
    binary = ''
    for char in message:
        binary += bin(ord(char))[2:].zfill(8)

    return binary


def bin2str(binary):
    message = ''
    for i in range(0, int(len(binary) / 8)):
        message += chr(int('' + binary[i * 8: i * 8 + 8], 2))
    return message


def encodeFile(filename, txt):
    img = Image.open(filename)
    imgData = img.getdata()
    newImgData = []

    binary = ''
    for line in txt:
        binary += str2bin(line)
    binary += '1111111111111110'
    index = 0

    flag = False

    for pixel in imgData:
        if not flag:
            newPixel = list(pixel)
            if (index != len(binary)):
                newPixel[0] = int(bin(pixel[0])[2:-1] + binary[index], 2)
                index += 1
            if (index != len(binary)):
                newPixel[1] = int(bin(pixel[1])[2:-1] + binary[index], 2)
                index += 1
            if (index != len(binary)):
                newPixel[2] = int(bin(pixel[2])[2:-1] + binary[index], 2)
                index += 1
            if (index != len(binary)):
                newPixel[3] = int(bin(pixel[3])[2:-1] + binary[index], 2)
                index += 1
            flag = index >= len(binary)
        else:
            newPixel = list(pixel)

        newImgData.append(tuple(newPixel))

    img.putdata(newImgData)
    img.save(filename[:-4] + '_encoded.png')


def encode(filename, message):
    img = Image.open(filename)
    imgData = img.getdata()
    newImgData = []

    binary = str2bin(message) + '1111111111111110'
    index = 0

    flag = False

    for pixel in imgData:
        if not flag:
            newPixel = list(pixel)
            if (index != len(binary)):
                newPixel[0] = int(bin(pixel[0])[2:-1] + binary[index], 2)
                index += 1
            if (index != len(binary)):
                newPixel[1] = int(bin(pixel[1])[2:-1] + binary[index], 2)
                index += 1
            if (index != len(binary)):
                newPixel[2] = int(bin(pixel[2])[2:-1] + binary[index], 2)
                index += 1
            if (index != len(binary)):
                newPixel[3] = int(bin(pixel[3])[2:-1] + binary[index], 2)
                index += 1
            flag = index >= len(binary)
        else:
            newPixel = list(pixel)

        newImgData.append(tuple(newPixel))

    img.putdata(newImgData)
    img.save(filename[:-4] + '_encoded.png')


def decode(filename):
    img = Image.open(filename)
    binary = ''

    imgData = img.getdata()

    for pixel in imgData:
        for channel in pixel:
            if len(binary) >= 16 and binary[-16:] == '1111111111111110':
                return bin2str(binary[:-16])

            binary += bin(channel)[2:].zfill(8)[-1]

    return bin2str(binary[:-16])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Encode / Decode information in an image')
    parser.add_argument('-e', '--encode', type=str, help='Encode a string to an image')
    parser.add_argument('-ef', '--encodeFile', type=str, help='Encode a .txt file to an image')
    parser.add_argument('-d', '--decode', type=str, help='Decode the info from the image')
    args = parser.parse_args()

    if args.encode != None:
        message = input("Enter the message: ")
        print('Encoding the above message into: %s' % (args.encode))
        encode(args.encode, message)
        print('Done. The encoded file is named %s' % (args.encode[:-4] + '_encoded.png'))

    elif args.encodeFile != None:
        txtName = input("Enter the .txt file you want to encode into the image: ")
        with open(txtName, 'r') as txt:
            message = txt.readlines()
        print('Encoding your message into: %s' % (args.encodeFile))
        encodeFile(args.encodeFile, message)
        print('Done. The encoded file is named %s' % (args.encodeFile[:-4] + '_encoded.png'))

    elif args.decode != None:
        print('Decoding the message from the image %s' % (args.decode))
        message = decode(args.decode)
        save = input('Done. \nDo you want to display the output on the screen, or save it into a text file?\nInput **file** for saving to .txt or **print** for printing it: ')
        if (save == 'file'):
            with open('message.txt', 'w') as txt:
                txt.writelines(message)
            print('Message was saved with the name message.txt')
        else:
            print(message)
